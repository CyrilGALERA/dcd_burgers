<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" sizes="16x16" href="./images/logo.png">
    <link rel="stylesheet" href="./styleG.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Praise&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
    <title>LOGIN</title>
</head>

<body>
    <header>
        <div>
            <div>
                <img src="./images/logo.png" alt="logo">
            </div>
            <h1> Dead Cow Dinner </h1>
        </div>

        <nav>
            <ul>
                <li>
                    <a class="Nav_Page_i" href="./carte.php">Notre Carte</a>
                </li>
                <li>
                    <a class="Nav_Page_i" href="./info_rest.php">Notre Restaurant</a>
                </li>
                <li>
                    <a class="Nav_Page_i" href="./reserver.php">Réserver une table</a>
                </li>
                <li>
                    <a class="Nav_Page_i" href="./carte.php">Commander chez Uber Eat</a>
                </li>
            </ul>
        </nav>

    </header>
    <main>
        <h2> - Administration - </h2>
        <div class="logadmin">
            <div class="logimg">
                <form name="form" id="formLogin" method="post" action="./admin.php">
                    <label id="labeltxt" for="pseudo">Login</label>
                    <input autocomplete="off" type="text" name="pseudo" placeholder="Id Admin" /><br />
                    <label id="labeltxt" for="password">Mot De Passe</label>
                    <input autocomplete="off" type="password" name="password" placeholder="Mot de passe" /><br />
                    <input type="submit" name="valider" id="btn_auth" value="S'authentifier" />
            </div>
        </div>
    </main>
</body>

</html>